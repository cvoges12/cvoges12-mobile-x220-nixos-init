{ config, pkgs, ... }:

{
  imports = [
    ./hardware.nix
  ];

  environment.systemPackages = with pkgs; [
    git
  ];

  services.xserver = {
    layout = "us";
    xkbVariant = "colemak";
  };
  console.useXkbConfig = true;

  services.openssh = {
    enable = true;
    permitRootLogin = "yes";
    ports = [ 2222 ];
  };

  networking.hostName = "minimal-x86-nixos";

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDO4/LsD0MJj8hl3w4mBEYiN1b0N4++I0fKje8fwzvas2fkli2w0s5GsAXE5eyTf/ksZcYGU4SIzx07K1QRnikEHLbGcWRefiBAZBbAYVEBIEiE2OJb4pfEQRFvTL5J8eRAtQjX+CwoTcRHEA/uZ/8TWQdSlq/OkfCfZOgx7K54noudkLkwO4/CqmbQvV8Bp9bxbc9Zc8LEpFOJrAJFbGUb6KDWpWdB2DhGuWVtvuQ1XOs3HJAJJo1BZCJ2Q/iieGumfCZm61AZ1vx5FyVaLxlL3rYiXJ0fRvBybACmkoAfmKCqkDNbVO78lWcPkgCftGYIpm8q0/XQGd28RnfTGiTOBLYCCQEzv5YYjSTuy8vgmIyihdsz2i387NyJbdeqgisR49TGCmlvGiWHJEgFEN5Lcrtqol2jsEA15RNghBgzZEjbMLze33iWHKTWzkzy3e/4PhyUAmWStvjpDVHoz5rGXzVxjF6QEUDDHqkK4qYxzUL/NrObw1wdSula3hsL7ds="
  ];

  nix.settings.experimental-features = [
    "nix-command"
    "flakes"
    "repl-flake"
  ];

  system.stateVersion = "22.11";
}
