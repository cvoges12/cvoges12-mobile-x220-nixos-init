{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    #kernelPackages = pkgs.linuxPackages_latest;
    #supportedFilesystems = lib.mkForce [
    #  "btrfs"
    #  "reiserfs"
    #  "vfat"
    #  "f2fs"
    #  "xfs"
    #  "ntfs"
    #  "cifs"
    #];
    loader = {
      grub = {
        enable = true;
        devices = [ "/dev/sda1" ];
        version = 2;
        efiSupport = true;
        efiInstallAsRemovable = true;
      };
    };
    initrd = {
      availableKernelModules = [
        "ehci_pci"
        "ahci"
        "usb_storage"
        "sd_mod"
        "sdhci_pci"
      ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
  };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
