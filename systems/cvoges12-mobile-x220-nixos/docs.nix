{ config, pkgs, ... }:

{
  documentation = {
    enable = true;
    man.enable = true;
    nixos.enable = true;
    dev.enable = true;
    doc.enable = true;
    info.enable = true;
  };
}
