{ config, pkgs, ... }:

{
  imports = [
    ./hardware.nix
    ./security.nix
    ./network.nix
    ./keymap.nix
    ./ssh.nix
    ./docs.nix
    ./locale+time.nix
    ./fonts.nix
    ./tmpwatch.nix
    ./audio.nix
    ./power.nix
  ];

  services = {
    printing.enable = true;
    fprintd.enable = true;

    udev.packages = with pkgs; [
      android-udev-rules
      yubikey-personalization
    ];
  };

  programs = {
    command-not-found.enable = true;
  };

  users.users.cvoges12 = {
    isNormalUser = true;
    description = "cvoges12";
    extraGroups = [
      "networkmanager"
      "wheel"
      "disk"
      "audio"
      "video"
      "libvirtd"
      "systemd-journal"
    ];
    packages = with pkgs; [];
  };

  environment = {
    shells = with pkgs; [ bashInteractive ];
    systemPackages = with pkgs; [
      git
      wget
      curl
      qpwgraph
      htop
      tree
      whois
      netcat
      nmap
      rsync
      testdisk
      ripgrep
      zip
      unzip
      xz
    ];
  };

  nixpkgs.config.allowUnfree = true;

  nix = {
    gc = {
      automatic = true;
      dates = "8:30";
    };

    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
        "repl-flake"
      ];

      auto-optimise-store = true;
    };
  };

  system.stateVersion = "22.11";
}
