{ config, pkgs, ... }:

{
  imports = [
    ./sway.nix
  ];

  home = {
    username = "cvoges12";
    homeDirectory = "/home/cvoges12";
    stateVersion = "22.11";

    packages = with pkgs; [
      # audio controls
      pamixer
      pavucontrol

      # music
      #ardour  # daw
      #musescore   # music score
      caudec  # audio file conversion
      asciinema
      bat
      debootstrap
      entr
      weechat
      units   # convert units
      ffmpeg
      fzf   # fuzzy file search

      # art programs
      inkscape
      gimp
      #krita  # uncomment when I'm actually willing to learn it

      kdeconnect
      harfbuzz
      hunspell
      libreoffice
      neofetch
      obs-studio
      zathura
      signal-desktop
      strace
      #virtmanager
      #qemu

      # dev tools
      clang
      openjdk

      waydroid

      #swhkd    # whenever it gets packaged
      keyd    # key daemon

      mpv
    ];
  };

  programs = {
    home-manager.enable = true;
    kitty.enable = true;
    qutebrowser = {
      enable = true;
      keyMappings = {};
      quickmarks = {};
      searchEngines = {};
      settings = {};
    };
    firefox = {
      enable = true;
      #profiles
    };
    yt-dlp.enable = true;
    newsboat = {
      enable = true;
      autoReload = true;
    };
  };

  services = {
    network-manager-applet.enable = true;
  };
}
