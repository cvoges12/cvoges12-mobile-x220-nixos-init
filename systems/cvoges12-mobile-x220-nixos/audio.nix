{ config, pkgs, ... }:

{
  sound = {
    enable = true;
    mediaKeys = {
      enable = true;
      volumeStep = "1";
    };
  };

  services.pipewire = {
    enable = true;
    systemWide = true;
    jack.enable = true;
    alsa.enable = true;
    pulse.enable = true;
    audio.enable = true;
    wireplumber.enable = true;
  };
}
