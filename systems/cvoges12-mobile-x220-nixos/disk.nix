{
  disk.sda = {
    device = "/dev/sda";
    type = "disk";
    content = {
      type = "table";
      format = "gpt";
      partitions = [
        {
          name = "boot";
          type = "partition";
          start = "0";
          end = "1M";
          part-type = "primary";
          flags = ["bios_grub"];
        }
        {
          name = "ESP";
          type = "partition";
          start = "1MiB";
          end = "512MiB";
          bootable = true;
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot";
          };
        }
        {
          name = "root";
          type = "partition";
          start = "512MiB";
          end = "-1G";
          part-type = "primary";
          bootable = true;
          content = {
            #type = "btrfs";
            #extraArgs = [ "-f" ];
            #subvolumes = {
            #  "root" = {
            #    mountpoint = "/";
            #  };
            #  "/home" = {
            #    mountOptions = [ "compress=zstd" ];
            #  };
            #  "/nix" = {
            #    mountOptions = [ "compress=zstd" "noatime" ];
            #  };
            #};
            type = "filesystem";
            format = "ext4";
            mountpoint = "/";
          };
        }
        {
          name = "swap";
          type = "partition";
          start = "-1G";
          end = "100%";
          part-type = "primary";
          content = {
            type = "swap";
            randomEncryption = true;
          };
        }
      ];
    };
  };
  nodev."/" = {
    fsType = "tmpfs";
    mountOptions = [
      "size=2G"
      "defaults"
      "mode=755"
    ];
  };
}
