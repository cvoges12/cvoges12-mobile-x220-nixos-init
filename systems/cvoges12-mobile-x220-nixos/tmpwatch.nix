{ config, pkgs, ... }:

{
  environment.systemPackages = [ pkgs.tmpwatch ];

  services.cron = {
    enable = true;
    systemCronJobs = [
      "0 0 1 * *     root     tmpwatch -maf 240 /tmp"
    ];
  };
}
