{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot = {
    loader = {
      #systemd-boot.enable = true;
      #efi = {
      #  canTouchEfiVariables = true;
      #  efiSysMountPoint = "/boot/efi";
      #};
      grub = {
        enable = true;
        devices = [ "/dev/sda1" ];
        version = 2;
        efiSupport = true;
        efiInstallAsRemovable = true;
      };
    };
    initrd = {
      availableKernelModules = [
        "ehci_pci"
        "ahci"
        "usb_storage"
        "sd_mod"
        "sdhci_pci"
      ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
  };

  #fileSystems = {
  #  "/" = {
  #    device = "/dev/disk/by-uuid/088705ce-d2fa-4d67-b4d8-3dee9502e07f";
  #    fsType = "ext4";
  #  };

  #  "/boot/efi" = {
  #    device = "/dev/disk/by-uuid/6C7E-D6CA";
  #    fsType = "vfat";
  #  };
  #};

  #swapDevices = [
  #  { device = "/dev/disk/by-uuid/672ff459-dc8a-4339-8da9-c2d0b50f9676"; }
  #];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted
  # networking (the default) this is the recommended approach. When using
  # systemd-networkd, it's still possible to use this option. But it's
  # recommended to use it in conjunction with explicit per-interface
  # declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp0s25.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp3s0.useDHCP = lib.mkDefault true;
  # networking.interfaces.wwp0s29u1u4.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
