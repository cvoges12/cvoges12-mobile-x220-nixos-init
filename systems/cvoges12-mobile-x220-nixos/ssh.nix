{ config, pkgs, ... }:

{
  services.openssh = {
    enable = true;
    ports = [ 2222 ];
    permitRootLogin = "no";
    allowSFTP = true;
  };
}
