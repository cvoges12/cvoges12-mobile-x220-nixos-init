{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    swaybg
    #mako  # notification daemon if dunst doesn't work
    dunst    # looks like it works on wayland
    imv   # wayland feh or sxiv
    #swayimg
    fuzzel
    swayr   # workspace switcher
    swaylock
    wayshot
  ];

  programs = {
    waybar = {
      enable = true;
      systemd = {
        enable = true;
        target = "sway-session.target";
      };
    };
  };

  services = {
    # uncomment at v23.05
    #clipman = {
    #  enable = true;
    #  systemdTarget = "sway-session.target";
    #};
    kanshi.enable = true;
    swayidle.enable = true;
  };

  wayland.windowManager.sway = {
    enable = true;
    systemdIntegration = true;
    config = {
      modifier = "Mod4";
    };
    swaynag.enable = true;
    xwayland = true;
  };
}
