{
  description = "cvoges12's nixos configurations";

  inputs = {
    nixpkgs.url = github:nixos/nixpkgs/nixos-unstable;
    disko = {
      url = github:nix-community/disko;
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = github:nix-community/home-manager;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs@{ self, nixpkgs, home-manager, disko, ... }: {
    nixosConfigurations = let
      system-config = { system, arch, ... }: nixpkgs.lib.nixosSystem {
        system = arch;
        specialArgs = {
          inherit self;
        };

        modules = [({
          imports = [
            (import "${self}/systems/${system}/configuration.nix")
            disko.nixosModules.disko
            home-manager.nixosModules.home-manager
          ];

          disko.devices = import "${self}/systems/${system}/disk.nix";

          home-manager = {
            useGlobalPkgs = true;
            useUserPackages = true;
            users.cvoges12 = import "${self}/systems/${system}/home.nix";
          };
        })];
      };
    in {
      mobile-x220 = system-config {
        system = "cvoges12-mobile-x220-nixos";
        arch = "x86_64-linux";
      };

      minimal-x64 = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";

        specialArgs = {
          inherit self;
        };

        modules = [({
          imports = [
            (import "${self}/systems/minimal-x64/configuration.nix")
            disko.nixosModules.disko
          ];

          disko.devices = import "${self}/systems/minimal-x64/disk.nix";
        })];
      };
    };
  };
}
