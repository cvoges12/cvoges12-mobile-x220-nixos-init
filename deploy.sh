#!/bin/sh

# $1 = ip address
# $2 = port number
# $3 = system config
mkdir -p ~/.ssh/
cp "$(dirname $0)/id_rsa" ~/.ssh/
nix run github:numtide/nixos-anywhere -- "ssh://root@$1:$2" --flake "gitlab:cvoges12/cvoges12-nixos-init#$3"
